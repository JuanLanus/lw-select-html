const flowers = {
	"AbutilonXHybridum":
     { "header": "Abutilon x hybridum", "title": "Abutilon flowers in a range of colours, from bright red and oranges, to lovely pinks pastels and whites.", "name": "AbutilonXHybridum" },
	"AcanthusMollis":
     { "header": "Acanthus mollis", "title": "If you have the space in your garden Acanthus mollis is one plant to choose. ", "name": "AcanthusMollis" },
	"AchilleaAgeratumMoonwalker":
     { "header": "Achillea ageratum 'Moonwalker'", "title": "The rich green foliage has a fern-like appearance that is just as much the star as those wonderful golden umbels. ", "name": "AchilleaAgeratumMoonwalker" },
	"AchilleaFilipendulinaClothOfGold":
     { "header": "Achillea filipendulina 'Cloth of Gold'", "title": "Achillea filipendulina ‘Cloth of Gold’ requires full sun for best flower production, but, this is little to ask for such a grand reward. ", "name": "AchilleaFilipendulinaClothOfGold" },
	"AchilleaMillefoliumCeriseQueen":
     { "header": "Achillea millefolium 'Cerise Queen'", "title": "Achillea ‘Cerise Queen’ is a carefree and generously blooming perennial with flat-topped clusters of vibrant, magenta-pink flowers with tiny white centers. ", "name": "AchilleaMillefoliumCeriseQueen" },
	"AchilleaMillefoliumColoradoMix":
     { "header": "Achillea millefolium 'Colorado Mix'", "title": "Achillea 'Colorado Mix' produces clusters of flowers in shades of pink, red, yellow, white and apricot.  ", "name": "AchilleaMillefoliumColoradoMix" },
	"AchilleaMillefoliumF2SummerBerries":
     { "header": "Achillea millefolium 'F2 Summer Berries'", "title": "Achellea 'F2 Summer Berries' is a carefree and generously blooming perennial ", "name": "AchilleaMillefoliumF2SummerBerries" },
	"AchilleaMillefoliumF2SummerPastels":
     { "header": "Achillea millefolium 'F2 Summer Pastels'", "title": "With a soft mix of pastel coloured flowers ‘Summer Pastels’ flowers in abundance from spring to first frosts. ", "name": "AchilleaMillefoliumF2SummerPastels" },
	"AchilleaMillefoliumProa,Organic":
     { "header": "Achillea millefolium 'Proa', Organic", "title": "Achilleahave clustered flower heads of tiny white flowers that from a distance look like little patches of snow resting on the grass.", "name": "AchilleaMillefoliumProa,Organic" },
	"AchilleaMillefoliumYarrow":
     { "header": "Achillea millefolium 'Yarrow'", "title": "Achillea millefolium have clustered flower heads of tiny white flowers that from a distance look like little patches of snow resting on the grass.", "name": "AchilleaMillefoliumYarrow" },
	"AchilleaPtarmicaThePearl":
     { "header": "Achillea ptarmica 'The Pearl'", "title": "Achillea ptarmica 'The Pearl' flowers in only three months from seed.", "name": "AchilleaPtarmicaThePearl" },
	"AchilleaSibiricaVar.CamtschaticaLoveParade":
     { "header": "Achillea sibirica var. camtschatica 'Love Parade'", "title": "Achillea sibirica var. camtschatica 'Love Parade' has dense clusters of large flat-topped soft pink flowers", "name": "AchilleaSibiricaVar.CamtschaticaLoveParade" },
	"AcoelorrhapheWrightii,EvergladesPalm’":
     { "header": "Acoelorrhaphe wrightii, 'Everglades Palm’", "title": "Acoelorrhaphe wrightii a small to moderately tall palm with several, upright, slender trunks which form attractive tight clumps, clustered at the base", "name": "AcoelorrhapheWrightii,EvergladesPalm’" },
	"AconitumNapellusSubsp.VulgareAlbidum":
     { "header": "Aconitum napellus subsp. vulgare Albidum", "title": "Aconitum napellus subsp. vulgare Albidum is a superb white form of the most popular Monkshood. ", "name": "AconitumNapellusSubsp.VulgareAlbidum" },
	"AconitumNapellus ‘NewryBlue’":
     { "header": "Aconitum napellus ‘Newry Blue’", "title": "Aconitum napellus ‘Newry Blue’ is an outstanding old Irish cultivar with wonderful upright spires of the deepest rich blue.", "name": "AconitumNapellus ‘NewryBlue’" },
	"AgapanthusAfricanusGettyWhite":
     { "header": "Agapanthus africanus 'Getty White'", "title": "White Agapanthus is dazzling against a dark green backdrop. ", "name": "AgapanthusAfricanusGettyWhite" },
	"AgapanthusAfricanusBlue":
     { "header": "Agapanthus africanus Blue", "title": "The Blue African Lily is one of the aristocrats of the late summer garden", "name": "AgapanthusAfricanusBlue" },
	"AgapanthusComptoniiHeadbourneHybrids":
     { "header": "Agapanthus comptonii 'Headbourne Hybrids'", "title": "Agapanthus is a versatile subject that is excellent for cutting. ", "name": "AgapanthusComptoniiHeadbourneHybrids" },
	"AgapanthusPraecoxSsp.MinimusPeterPan":
     { "header": "Agapanthus praecox ssp. minimus 'Peter Pan'", "title": "Agapanthus praecox ssp. minimus 'Peter Pan'", "name": "AgapanthusPraecoxSsp.MinimusPeterPan" },
	"AgastacheAurantiaca ‘ApricotSprite’":
     { "header": "Agastache aurantiaca ‘Apricot Sprite’", "title": "Agastache ‘Apricot Sprite’ is an outstanding perennial that provides a sizzling blast of tubular, peachy-apricot flowers", "name": "AgastacheAurantiaca ‘ApricotSprite’" },
	"AgastacheAurantiaca ‘Tango’":
     { "header": "Agastache aurantiaca ‘Tango’", "title": "Agastache ‘Tango’ was bred specifically to tolerate wet or humid summers better than other cultivars.", "name": "AgastacheAurantiaca ‘Tango’" },
	"AgastacheFoeniculumGoldenJubilee":
     { "header": "Agastache foeniculum 'Golden Jubilee'", "title": "Agastache foeniculum 'Golden Jubilee' has one of the most remarkable leaf colour of any flowering plant. Bright gold-chartreuse.", "name": "AgastacheFoeniculumGoldenJubilee" },
	"AgastacheFoeniculum,AniseHyssop,Organic":
     { "header": "Agastache foeniculum, Anise Hyssop, Organic", "title": "Agastache foeniculum is a favourite plant of bees and gardeners alike, it is, perhaps one of the most gratifying plants you can grow.", "name": "AgastacheFoeniculum,AniseHyssop,Organic" },
	"AgastacheHybridaArcadoPink":
     { "header": "Agastache hybrida 'Arcado Pink'", "title": "Agastache ‘Arcado Pink’ is an extremely floriferous variety with lovely purple-pink flower spikes and fresh green aromatic foliage.", "name": "AgastacheHybridaArcadoPink" },
	"AgastacheHybridaAstelloIndigo":
     { "header": "Agastache hybrida 'Astello Indigo'", "title": "Agastache 'Astello Indigo' is just about the most exciting breakthrough in the Hummingbird Mint family yet ", "name": "AgastacheHybridaAstelloIndigo" },
	"AgastacheHybridaAsturoWhite":
     { "header": "Agastache hybrida 'Asturo White'", "title": "‘Asturo White’ feature scented white flowers with fresh lime green spikes that stand out from all other agastaches. ", "name": "AgastacheHybridaAsturoWhite" },
	"AgastacheHybrida, ‘Globetrotter’ ":
     { "header": "Agastache hybrida, ‘Globetrotter’ ", "title": "Agastache ‘Globetrotter’ have soft, touchable flower spikes that are lilac-pink with carmine-red bracts. ", "name": "AgastacheHybrida, ‘Globetrotter’ " },
	"AgastacheRugosaAlba, ‘LiquoriceWhite’ ":
     { "header": "Agastache rugosa alba, ‘Liquorice White’ ", "title": "Agastache 'Liquorice White' has tall spikes of white lipped flowers", "name": "AgastacheRugosaAlba, ‘LiquoriceWhite’ " },
	"AgastacheRugosa, ‘LiquoriceBlue’ ":
     { "header": "Agastache rugosa, ‘Liquorice Blue’ ", "title": "Agastache 'Liquorice Blue' has tall spikes of lavender to rich purpley blue lipped flowers", "name": "AgastacheRugosa, ‘LiquoriceBlue’ " },
	"AgaveMckelveyana,McKelveysCenturyPlant":
     { "header": "Agave mckelveyana, 'McKelvey's Century Plant'", "title": "Agave mckelveyana, 'McKelvey's Century Plant'", "name": "AgaveMckelveyana,McKelveysCenturyPlant" },
	"AgaveMixedSpecies":
     { "header": "Agave Mixed Species", "title": "Agave utahensis is an uncommon plant, native to the east of the Mojave Desert ", "name": "AgaveMixedSpecies" },
	"AgrostemmaGithago":
     { "header": "Agrostemma githago", "title": "Agrostemma githago", "name": "AgrostemmaGithago" },
	"AjugaGenevensis ":
     { "header": "Ajuga genevensis ", "title": "With deep true gentian blue flower spikes Ajuga genevensis is by far the showiest of the species.  ", "name": "AjugaGenevensis " },
	"AjugaReptans ":
     { "header": "Ajuga reptans ", "title": "For most of the year Ajuga is a pleasant quiet achiever, but those weeks in spring when the blue flowers appear are simply quite magical.", "name": "AjugaReptans " },
	"AlceaFicifoliaHappyLights":
     { "header": "Alcea ficifolia 'Happy Lights'", "title": "Alcea ficifolia produce many upright stems emerging from the base, resulting in a bushy form.", "name": "AlceaFicifoliaHappyLights" },
	"AlceaFicifoliaYellow":
     { "header": "Alcea ficifolia Yellow", "title": "Alcea ficifolia produces large, pale butter-yellow single saucer shaped flowers that are 8 to 12cm (3 to 5in) wide from May to October. ", "name": "AlceaFicifoliaYellow" },
	"AlceaRoseaChatersDoubleMix":
     { "header": "Alcea rosea 'Chaters Double Mix'", "title": "Alcea rosea ‘Chater’s Double’ give a wonderful mixed colour range - White, yellow, crimson, pink, purple, rose, and red. ", "name": "AlceaRoseaChatersDoubleMix" },
	"AlceaRoseaIndianSpring":
     { "header": "Alcea rosea 'Indian Spring'", "title": "If planted early in spring, Indian Spring will bloom the first year in wonderfully warm shades of ruby through pink and rose to white.", "name": "AlceaRoseaIndianSpring" },
	"AlceaRoseaQueenyMix":
     { "header": "Alcea rosea 'Queeny Mix'", "title": "Queen Mix at the New York Botanical Garden. ", "name": "AlceaRoseaQueenyMix" },
	"AlceaRoseaSawyersSingleMix":
     { "header": "Alcea rosea 'Sawyers Single Mix'", "title": "The dramatic, single flowers of this variety work equally well in a contemporary, minimalist garden.", "name": "AlceaRoseaSawyersSingleMix" },
	"AlceaRoseaVar.Nigra":
     { "header": "Alcea rosea var. 'Nigra'", "title": "The dramatic, near-black flowers of Nigra work equally well in a contemporary, minimalist garden. ", "name": "AlceaRoseaVar.Nigra" },
	"AlchemillaMollisLadysMantle":
     { "header": "Alchemilla mollis 'Lady's Mantle'", "title": "The unpretentious Lady's Mantle is extremely useful for both its foliage and its flowers. ", "name": "AlchemillaMollisLadysMantle" },
	"AlliumChristophiiStarOfPersia":
     { "header": "Allium christophii 'Star of Persia'", "title": "Allium christophii is the most flamboyant member of this enormous family of plants.", "name": "AlliumChristophiiStarOfPersia" },
	"AlliumHollandicumPurpleSensation":
     { "header": "Allium hollandicum 'Purple Sensation'", "title": "Allium 'Purple Sensation' is a stunning fashionable plant, with globes of rosy-purple crowded spherical umbels, and strap shaped leaves. ", "name": "AlliumHollandicumPurpleSensation" },
	"AlliumNarcissiflorum":
     { "header": "Allium narcissiflorum", "title": "Allium narcissiflorum produces large, pink-magenta, open-mouthed flowers each in the shape of a hand-bell,", "name": "AlliumNarcissiflorum" },
	"AlliumSphaerocephalon":
     { "header": "Allium sphaerocephalon", "title": "The tall slender stems are good at adding colour and movement to the garden, ", "name": "AlliumSphaerocephalon" },
	"AlliumUrsinum":
     { "header": "Allium ursinum", "title": "Ramsons Garlic", "name": "AlliumUrsinum" },
	"AlstroemeriaLigtuHybrids":
     { "header": "Alstroemeria 'Ligtu Hybrids'", "title": "Don't be fooled by the Alstroemeria's delicate appearance, these lily-like flowers are as long lasting as they are gorgeous.", "name": "AlstroemeriaLigtuHybrids" },
	"AmaranthusCaudatusLoveLiesBleedingRed":
     { "header": "Amaranthus caudatus 'Love Lies Bleeding' Red", "title": "Amaranthus caudatus is the hanging or drooping amaranthus, the deep red variety also known as Love-Lies-Bleeding.      ", "name": "AmaranthusCaudatusLoveLiesBleedingRed" },
	"AmaranthusCaudatusViridis":
     { "header": "Amaranthus caudatus viridis", "title": "Amaranthus caudatus ‘viridis’ is the gorgeous green form of the popular drooping amaranthus. ", "name": "AmaranthusCaudatusViridis" },
	"AmaranthusCruentusHotBiscuits":
     { "header": "Amaranthus cruentus 'Hot Biscuits'", "title": "Amaranthus cruentus 'Hot Biscuits'", "name": "AmaranthusCruentusHotBiscuits" },
	"AmaranthusPaniculatusOeschberg":
     { "header": "Amaranthus paniculatus 'Oeschberg'", "title": "Amaranthus paniculatus 'Oeschberg'", "name": "AmaranthusPaniculatusOeschberg" },
	"AmaranthusTricolourEarlySplendour":
     { "header": "Amaranthus tricolour 'Early Splendour'", "title": "Amaranthus tricolour Early Splendour is grown for its beautiful foliage. ", "name": "AmaranthusTricolourEarlySplendour" },
	"AmaranthusTricolourIllumination":
     { "header": "Amaranthus tricolour 'Illumination'", "title": "Amaranthus tricolour 'Illumination'", "name": "AmaranthusTricolourIllumination" },
	"AmaranthusTricolourSplendensJosephsCoat. ":
     { "header": "Amaranthus tricolour 'Splendens' Joseph's Coat. ", "title": "Amaranthus tricolour 'Splendens' feature colourful leaves in bright, stable tricolour blends of red, yellow and green.", "name": "AmaranthusTricolourSplendensJosephsCoat. " },
	"AmmiMajusQueenAnnesLace":
     { "header": "Ammi majus 'Queen Anne's Lace'", "title": "In summer, Ammi majus bears an abundance of large round blooms made up of clusters of tiny white florets. ", "name": "AmmiMajusQueenAnnesLace" },
	"AmmiVisnagaGreenMist":
     { "header": "Ammi visnaga 'Green Mist'", "title": "Ammi visnaga 'Green Mist' is a new variety of Queen Annes Lace, with larger, darker green umbels", "name": "AmmiVisnagaGreenMist" },
	"AmmiVisnaga,Organic":
     { "header": "Ammi visnaga, Organic", "title": "Ammi visnaga 'Green Mist' is a new variety of Queen Annes Lace, with larger, darker green umbels", "name": "AmmiVisnaga,Organic" },
	"AmpelodesmosMauritanicus":
     { "header": "Ampelodesmos mauritanicus", "title": "The lofty, arching stems race skyward, with impressive pearl-coloured flower panicles", "name": "AmpelodesmosMauritanicus" },
	"AmsoniaHubrichtii":
     { "header": "Amsonia hubrichtii", "title": "From a distance the steely blue flowers of Amsonia hubrichtii have an almost lily-like appearance. ", "name": "AmsoniaHubrichtii" },
	"AmsoniaTabernaemontana":
     { "header": "Amsonia tabernaemontana", "title": "One of the loveliest of Amsonia, Amsonia tabernaemontana 'Blue Star' is easier to cultivate than it is to pronounce", "name": "AmsoniaTabernaemontana" },
	"AnagallisMonellii":
     { "header": "Anagallis monellii", "title": "Anagallis monellii has one of the brightest gentian-blue flowers available.", "name": "AnagallisMonellii" },
	"AnchusaCapensisBlueAngel":
     { "header": "Anchusa capensis 'Blue Angel'", "title": "Easy to grow and trouble free, team Anchusa capensis 'Blue Angel' with other annuals or perennials for eye-popping combinations.", "name": "AnchusaCapensisBlueAngel" },
	"AnchusaItalicaDropmore":
     { "header": "Anchusa italica 'Dropmore'", "title": "Anchusa italica 'Dropmore' spikes of rich gentian blue, giant forget-me-not type flowers", "name": "AnchusaItalicaDropmore" },
	"AnchusaOfficinalis":
     { "header": "Anchusa officinalis", "title": "Noted for its deep sapphire-blue flowers that are extremely attractive to wildlife, Anchusa is a relative of borage.", "name": "AnchusaOfficinalis" },
	"AnemantheleLessonianaPheasant’sTailGrass":
     { "header": "Anemanthele lessoniana 'Pheasant’s Tail Grass'", "title": "From midsummer it produces airy panicles of purple-green flowers that that almost touch the ground. ", "name": "AnemantheleLessonianaPheasant’sTailGrass" },
	"AnemoneHupehensisJapaneseAnemone":
     { "header": "Anemone hupehensis 'Japanese Anemone'", "title": "Japanese Anemones are among the best late summer and autumn border flowers, providing colour late in the flowering season when many other plants are beginning to fade.", "name": "AnemoneHupehensisJapaneseAnemone" },
	"AnemoneNemorosaWoodAnemone":
     { "header": "Anemone nemorosa 'Wood Anemone'", "title": "Wood Anemones growing on a south-facing edge of a wood all turn their heads to follow the sun from east to west. ", "name": "AnemoneNemorosaWoodAnemone" },
	"AnemoneSylvestrisSnowdropWindflower":
     { "header": "Anemone sylvestris 'Snowdrop Windflower'", "title": "Affectionately known as the Snowdrop Anemone, Anemone sylvestris is a charming perennial with satiny white flowers.", "name": "AnemoneSylvestrisSnowdropWindflower" },
	"AnemonopsisMacrophylla ":
     { "header": "Anemonopsis macrophylla ", "title": "Anemonopsis macrophylla is one of the most beautiful and elegant plants that you could wish for. ", "name": "AnemonopsisMacrophylla " },
	"AnethumGraveolens,FloristsDill ‘Mariska’ ":
     { "header": "Anethum graveolens, Florists Dill ‘Mariska’ ", "title": "‘Mariska’ is a little known but hardy and compact variety of Dill. ", "name": "AnethumGraveolens,FloristsDill ‘Mariska’ " },
	"AngelicaArchangelica":
     { "header": "Angelica archangelica", "title": "Angelica archangelica is a majestic plant that deserves a prominent position at the back of a border or in a wild part of the garden. ", "name": "AngelicaArchangelica" },
	"AnnualDwarfFlowersMix.":
     { "header": "Annual Dwarf Flowers Mix.", "title": "Growing to a height of around 30cm (12in) the Dwarf Cut Flower mix give instant impact and a definite wow factor.", "name": "AnnualDwarfFlowersMix." },
	"AnnualSemi-TallFlowersMix.":
     { "header": "Annual Semi-Tall Flowers Mix.", "title": "The Semi-Tall Cut Flowers, Annual Mix is a richly flowering mixture that produces large quantities of flowers for cutting.", "name": "AnnualSemi-TallFlowersMix." },
	"AnnualTallFlowersMix":
     { "header": "Annual Tall Flowers Mix", "title": "Growing to a height of around 60 to 70cm (24 to 28in), the Tall Cut Flowers, Annual Mix produces large quantities of flowers for cutting. ", "name": "AnnualTallFlowersMix" },
	"AnthriscusSylvestrisCowParsley":
     { "header": "Anthriscus sylvestris 'Cow Parsley'", "title": "Our native Cow Parsley has a sophisticated form, with delicate, open, white lacy umbels, they look as though they're erupting from a well shaken champagne bottle. ", "name": "AnthriscusSylvestrisCowParsley" },
	"AnthriscusSylvestrisRavenswing":
     { "header": "Anthriscus sylvestris 'Ravenswing'", "title": "Anthriscus sylvestris 'Ravenswing' has stunning, rich deep purple, almost black, finely cut ferny foliage. ", "name": "AnthriscusSylvestrisRavenswing" },
	"AntirrhinumMajusAppleblossom ":
     { "header": "Antirrhinum majus 'Appleblossom' ", "title": "‘Appleblossom’ offers soft, soft, pastel-toned blooms in peach, apricot and soft pink that are beautifully blended", "name": "AntirrhinumMajusAppleblossom " },
	"AntirrhinumMajusDefiance ":
     { "header": "Antirrhinum majus 'Defiance' ", "title": "Antirrhinum majus 'Defiance' ", "name": "AntirrhinumMajusDefiance " },
	"AntirrhinumMajusOrangeWonder ":
     { "header": "Antirrhinum majus 'Orange Wonder' ", "title": "Antirrhinum  'Orange Wonder' has the most beautiful and delicate, orange-rose blooms. ", "name": "AntirrhinumMajusOrangeWonder " },
	"AntirrhinumMajusRose ":
     { "header": "Antirrhinum majus 'Rose' ", "title": "Antirrhinum majus 'Rose' ", "name": "AntirrhinumMajusRose " },
	"AntirrhinumMajusRuby ":
     { "header": "Antirrhinum majus 'Ruby' ", "title": "Antirrhinum majus 'Ruby' produces densely packed spikes that bloom in succession with voluptuous, rich ruby red flowers. ", "name": "AntirrhinumMajusRuby " },
	"AntirrhinumMajusSnowflake ":
     { "header": "Antirrhinum majus 'Snowflake' ", "title": "Blooming from early summer right through to mid-autumn, they are an all-around excellent garden performer. ", "name": "AntirrhinumMajusSnowflake " },
	"AntirrhinumMajusTetraMixed ":
     { "header": "Antirrhinum majus 'Tetra Mixed' ", "title": "Antirrhinum majus ‘Tetra Mix’ produce tall, stately plants with ruffled blooms in a rich variety of colours. ", "name": "AntirrhinumMajusTetraMixed " },
	"AntirrhinumPumilumMagicCarpetMixed":
     { "header": "Antirrhinum pumilum 'Magic Carpet Mixed'", "title": "The Magic Carpet Snapdragon is one of our most familiar flowers, they have been garden favourites for generations ", "name": "AntirrhinumPumilumMagicCarpetMixed" },
	"AquilegiaAlpina":
     { "header": "Aquilegia alpina", "title": "Aquilegia alpina is a species-columbine that rivals the fanciest cultivars.", "name": "AquilegiaAlpina" },
	"AquilegiaAtrata":
     { "header": "Aquilegia atrata", "title": "Aquilegia atrata is a beautiful free flowering species that is native to the alpine meadows and forest clearings of Switzerland and Northern Europe. ", "name": "AquilegiaAtrata" },
	"AquilegiaCanadensis":
     { "header": "Aquilegia canadensis", "title": "This superb variety is quite unlike the usual Aquilegia with dark-green foliage and eye-catching, scarlet and lemon-yellow flowers", "name": "AquilegiaCanadensis" },
	"AquilegiaChrysantha ‘YellowQueen’":
     { "header": "Aquilegia chrysantha ‘Yellow Queen’", "title": "Aquilegia chrysantha ‘Golden Queen’ has large soft golden-yellow blooms, each with long swept back spurs", "name": "AquilegiaChrysantha ‘YellowQueen’" },
	"AquilegiaClematiflora ":
     { "header": "Aquilegia clematiflora ", "title": "The extravagantly multi-petalled blooms with starburst points are quite different from any other columbines.", "name": "AquilegiaClematiflora " },
	"AquilegiaFlabellataMini-Star":
     { "header": "Aquilegia flabellata 'Mini-Star'", "title": "Mini-Star is a delightful dwarf cultivar with bright blue flowers with a white corolla and stands only 15 to 20cm tall.", "name": "AquilegiaFlabellataMini-Star" },
	"AquilegiaVar.StellataBarlowMix":
     { "header": "Aquilegia var. stellata 'Barlow Mix'", "title": "Aquilegia var. stellata 'Barlow Mix' is the first Aquilegia series with fully double, spurless flowers.", "name": "AquilegiaVar.StellataBarlowMix" },
	"AquilegiaVulgarisWilliamGuinness":
     { "header": "Aquilegia vulgaris 'William Guinness'", "title": "Aquilegia vulgaris 'William Guinness'", "name": "AquilegiaVulgarisWilliamGuinness" },
	"AquilegiaVulgarisVar.StellataNoraBarlow":
     { "header": "Aquilegia vulgaris var. stellata 'Nora Barlow'", "title": "Nora Barlow is a modern name attached to this ancient type of  &quot;rose&quot; columbine", "name": "AquilegiaVulgarisVar.StellataNoraBarlow" },
	"AquilegiaXHybridaDragonfly":
     { "header": "Aquilegia x hybrida 'Dragonfly'", "title": "Aquilegia x hybrida 'Dragonfly'", "name": "AquilegiaXHybridaDragonfly" },
	"AquilegiaXHybridaMckanaGiantsMix":
     { "header": "Aquilegia x hybrida 'Mckana Giants Mix'", "title": "Aquilegia x hybrida 'Mckana Giants Mix'", "name": "AquilegiaXHybridaMckanaGiantsMix" },
	"AquilegiaXHybrida,BlueStar":
     { "header": "Aquilegia x hybrida, 'Blue Star'", "title": "Aquilegia x hybrida, 'Blue Star'", "name": "AquilegiaXHybrida,BlueStar" },
	"AquilegiaXHybrida,RedStar":
     { "header": "Aquilegia x hybrida, 'Red Star'", "title": "Aquilegia x hybrida, 'Red Star'", "name": "AquilegiaXHybrida,RedStar" },
	"AquilegiaXHybrida,WhiteStar":
     { "header": "Aquilegia x hybrida, 'White Star'", "title": "Aquilegia x hybrida, 'White Star'", "name": "AquilegiaXHybrida,WhiteStar" },
	"ArabisAlpinaSubsp.CaucasicaSnowcap":
     { "header": "Arabis alpina subsp. caucasica 'Snowcap'", "title": "Plant Arabis under spring-blooming bulbs with Narcissus, Primula veris or Viola. ", "name": "ArabisAlpinaSubsp.CaucasicaSnowcap" },
	"ArabisBlepharophyllaSpringCharm":
     { "header": "Arabis blepharophylla 'Spring Charm'", "title": "A well grown specimen can be covered in hundreds of flowers which are sweetly scented and long-lasting.", "name": "ArabisBlepharophyllaSpringCharm" },
	"ArctotisHarlequinMixed":
     { "header": "Arctotis 'Harlequin Mixed'", "title": "Arctotis was born to play a starring role. They are among the most brilliant of all the daisy flowers. ", "name": "ArctotisHarlequinMixed" },
	"ArenariaMontanaMountainSandwort ":
     { "header": "Arenaria montana 'Mountain Sandwort' ", "title": "Arenaria montana is a classic little alpine or rock garden plant, still relatively unknown to many gardeners. ", "name": "ArenariaMontanaMountainSandwort " },
	"ArmeriaMaritimaAlba":
     { "header": "Armeria maritima 'Alba'", "title": "Armeria maritima, our well-known little “Thrift”, which grows so happily on cliffs and seashores is also a very popular garden flower.", "name": "ArmeriaMaritimaAlba" },
	"ArmeriaMaritimaSplendens":
     { "header": "Armeria maritima 'Splendens'", "title": "This compact, evergreen perennial grows in low clumps and sends up long stems from which globes of bright pink blooms. ", "name": "ArmeriaMaritimaSplendens" },
	"ArmeriaMaritimaThrift":
     { "header": "Armeria maritima 'Thrift'", "title": "Armeria maritima, our well-known little “Thrift”, which grows so happily on cliffs and seashores is also a very popular garden flower. ", "name": "ArmeriaMaritimaThrift" },
	"ArtemisiaLudoviciana":
     { "header": "Artemisia ludoviciana", "title": "The grey-white stems of Artemisia ludoviciana bear fine fuzzy silvery foliage that is fragrant and soft.", "name": "ArtemisiaLudoviciana" },
	"ArtemisiaStellerianaMorisStrain":
     { "header": "Artemisia stelleriana 'Mori's Strain'", "title": "Artemisia stelleriana 'Mori's Strain', also known as 'Boughton Silver' is a superior, dense, mat-forming selection.", "name": "ArtemisiaStellerianaMorisStrain" },
	"ArumItalicumSubsp.Italicum. ":
     { "header": "Arum italicum subsp. italicum. ", "title": "As spring approaches and the weather warms, unusual creamy-white spathes (flowers) up to 40cm (16in) in length emerge.", "name": "ArumItalicumSubsp.Italicum. " },
	"AruncusDioicus,GoatsBeard":
     { "header": "Aruncus dioicus, Goat's Beard", "title": "Aruncus dioicus, Goat's Beard", "name": "AruncusDioicus,GoatsBeard" },
	"AsphodelineLutea ":
     { "header": "Asphodeline lutea ", "title": "Asphodeline lutea is an easy to grow perennial that has great architectural form. ", "name": "AsphodelineLutea " },
	"AstrantiaMajorPrimadonna":
     { "header": "Astrantia major 'Primadonna'", "title": "Astrantia major 'Primadonna'", "name": "AstrantiaMajorPrimadonna" },
	"AstrantiaMajorAlba":
     { "header": "Astrantia major alba", "title": "Astrantia major alba", "name": "AstrantiaMajorAlba" },
	"AubrietaDeltoideaRoyalBlue":
     { "header": "Aubrieta deltoidea 'Royal Blue'", "title": "Aubrieta deltoidea 'Royal Blue'", "name": "AubrietaDeltoideaRoyalBlue" },
	"AubrietaDeltoideaRoyalRed":
     { "header": "Aubrieta deltoidea 'Royal Red'", "title": "Aubrieta ‘Royal Red’ forms a low cushion of evergreen leaves, smothered by red flowers for several weeks.", "name": "AubrietaDeltoideaRoyalRed" },
	"AubrietaDeltoideaRoyalSeriesMix":
     { "header": "Aubrieta deltoidea 'Royal Series Mix'", "title": "Aubrieta deltoidea flower so prolifically, that the blossoms almost completely cover the foliage below.", "name": "AubrietaDeltoideaRoyalSeriesMix" },
	"AubrietaDeltoideaRoyalViolet":
     { "header": "Aubrieta deltoidea 'Royal Violet'", "title": "In spring Aubrieta 'Royal Violet' flower so prolifically, that the blossoms almost completely cover the foliage below.", "name": "AubrietaDeltoideaRoyalViolet" },
	"AubrietaHybridaHendersonii":
     { "header": "Aubrieta hybrida 'Hendersonii'", "title": "Aubrieta hybrida 'Hendersonii' is a vigorous variety that is smothered by rich lilac-purple flowers for several months in spring.", "name": "AubrietaHybridaHendersonii" },
	"AubrietaHybridaWhitewellGem":
     { "header": "Aubrieta hybrida 'Whitewell Gem'", "title": "Aubrieta ‘Whitewell Gem’ has a ground-hugging habit and freely produces large, intense reddish-purple flowers.for several months.", "name": "AubrietaHybridaWhitewellGem" },
	"AubrietaXCultorumCascadeMix":
     { "header": "Aubrieta x cultorum 'Cascade Mix'", "title": "Aubrieta x cultorum 'Cascade Mix'", "name": "AubrietaXCultorumCascadeMix" },
	"BellisPerennis ‘PomponetteMix’":
     { "header": "Bellis perennis ‘Pomponette Mix’", "title": "Pomponette Mix boasts masses of tightly quilled 4cm button, fully double flowers on neat, compact plants.", "name": "BellisPerennis ‘PomponetteMix’" },
	"BellisPerennis ‘PomponetteRed’":
     { "header": "Bellis perennis ‘Pomponette Red’", "title": "Bellis perennis ‘Pomponette Red’", "name": "BellisPerennis ‘PomponetteRed’" },
	"BellisPerennis ‘PomponetteShadesOfRose’":
     { "header": "Bellis perennis ‘Pomponette Shades of Rose’", "title": "Bellis &quot;Pomponette Rose&quot; is widely cultivated and prized for it's long lasting early spring blooms.", "name": "BellisPerennis ‘PomponetteShadesOfRose’" },
	"BellisPerennis ‘PomponetteWhite’":
     { "header": "Bellis perennis ‘Pomponette White’", "title": "Bellis perennis ‘Pomponette White’", "name": "BellisPerennis ‘PomponetteWhite’" },
	"BergeniaCordifolia NewHybrids ":
     { "header": "Bergenia cordifolia ' New Hybrids' ", "title": "Bergenia cordifolia is one of the most useful of all plants and worthy of a place in any garden.", "name": "BergeniaCordifolia NewHybrids " },
	"BergeniaCordifoliaRedBeauty":
     { "header": "Bergenia cordifolia 'Red Beauty'", "title": "Bergenia cordifolia 'Red Beauty' produce clusters or sprays of large red bell-shaped flowers", "name": "BergeniaCordifoliaRedBeauty" },
	"BetonicaOfficinalis,Betony":
     { "header": "Betonica officinalis, Betony", "title": "Betonica officinalis, Betony", "name": "BetonicaOfficinalis,Betony" },
	"Borage,BlueBorage,BoragoOfficinalis":
     { "header": "Borage, Blue Borage, Borago officinalis", "title": "Blue flowers are always welcome in the garden: Borage often flowers lavishly about 8 weeks after sowing.", "name": "Borage,BlueBorage,BoragoOfficinalis" },
	"Borage,Blue,BoragoOfficinalis,Organic":
     { "header": "Borage, Blue, Borago officinalis, Organic", "title": "Blue flowers are always welcome in the garden: Borage often flowers lavishly about 8 weeks after sowing.", "name": "Borage,Blue,BoragoOfficinalis,Organic" },
	"Borage,WhiteBorage,BoragoOfficinalisAlba":
     { "header": "Borage, White Borage, Borago officinalis alba", "title": "Not so well known is the beautiful white form of Borage with pure white flowers.", "name": "Borage,WhiteBorage,BoragoOfficinalisAlba" },
	"BrachycomeIberidifoliaMix":
     { "header": "Brachycome iberidifolia Mix", "title": "Brachycome iberidifolia Mix", "name": "BrachycomeIberidifoliaMix" },
	"Brassica,OrnamentalF1ChidoriRed":
     { "header": "Brassica, Ornamental 'F1 Chidori Red'", "title": "Brassica, Ornamental 'F1 Chidori Red'", "name": "Brassica,OrnamentalF1ChidoriRed" },
	"Brassica,OrnamentalF1ChidoriWhite":
     { "header": "Brassica, Ornamental 'F1 Chidori White'", "title": "Brassica, Ornamental 'F1 Chidori White'", "name": "Brassica,OrnamentalF1ChidoriWhite" },
	"Brassica,OrnamentalF1PeacockRed":
     { "header": "Brassica, Ornamental 'F1 Peacock Red'", "title": "'Red Peacock' has crinkly leaves almost coral-like in their intricacy and with colours of delicate pink through to vibrant red,", "name": "Brassica,OrnamentalF1PeacockRed" },
	"Brassica,OrnamentalF1PeacockWhite":
     { "header": "Brassica, Ornamental 'F1 Peacock White'", "title": "The ornamental cabbage is the perfect hardy annual for introducing sculptural form and a splash of vibrant colour to a winter garden.", "name": "Brassica,OrnamentalF1PeacockWhite" },
	"Brassica,OrnamentalF1PigeonMixed":
     { "header": "Brassica, Ornamental 'F1 Pigeon Mixed'", "title": "Brassica, Ornamental 'F1 Pigeon Mixed'", "name": "Brassica,OrnamentalF1PigeonMixed" },
	"Brassica,OrnamentalF1PigeonRed":
     { "header": "Brassica, Ornamental 'F1 Pigeon Red'", "title": "Brassica, Ornamental 'F1 Pigeon Red'", "name": "Brassica,OrnamentalF1PigeonRed" },
	"Brassica,OrnamentalF1PigeonVictoria":
     { "header": "Brassica, Ornamental 'F1 Pigeon Victoria'", "title": "Brassica, Ornamental 'F1 Pigeon Victoria'", "name": "Brassica,OrnamentalF1PigeonVictoria" },
	"Brassica,OrnamentalF1PigeonWhite":
     { "header": "Brassica, Ornamental 'F1 Pigeon White'", "title": "Brassica, Ornamental 'F1 Pigeon White'", "name": "Brassica,OrnamentalF1PigeonWhite" },
	"Brassica,OrnamentalF1TokyoMixed":
     { "header": "Brassica, Ornamental 'F1 Tokyo Mixed'", "title": "Brassica, Ornamental 'F1 Tokyo Mixed'", "name": "Brassica,OrnamentalF1TokyoMixed" },
	"BrizaMaximaGreaterQuakingGrass":
     { "header": "Briza maxima 'Greater Quaking Grass'", "title": "Briza has flower heads hang like scaly little heart shaped lockets from late spring to mid summer.", "name": "BrizaMaximaGreaterQuakingGrass" },
	"BrizaMediaQuakingGrass":
     { "header": "Briza media 'Quaking Grass'", "title": "Worthy of a place in any border", "name": "BrizaMediaQuakingGrass" },
	"BrugmansiaArboreaAlbaAngelsTrumpets":
     { "header": "Brugmansia arborea alba 'Angels Trumpets'", "title": "Brugmansia arborea alba 'Angels Trumpets' have lush foliage and masses of delightfully fragrant white trumpets. ", "name": "BrugmansiaArboreaAlbaAngelsTrumpets" },
	"BrugmansiaSuaveolensPinkAngelsTrumpets":
     { "header": "Brugmansia suaveolens Pink 'Angels Trumpets'", "title": "Brugmansia suaveolens are large shrubs with lush foliage and masses of delightfully fragrant trumpets. ", "name": "BrugmansiaSuaveolensPinkAngelsTrumpets" },
	"BuddleiaDavidii,MixedColours":
     { "header": "Buddleia davidii, Mixed Colours", "title": "Few plants shine as brightly in late summer as the buddleia. ", "name": "BuddleiaDavidii,MixedColours" },
	"BupleurumRotundifoliumGriffithii":
     { "header": "Bupleurum rotundifolium 'Griffithii'", "title": "Bupleurum rotundifolium 'Griffithii'", "name": "BupleurumRotundifoliumGriffithii" },
	"CimicifugaRacemosaVar.Cordifolia":
     { "header": "Cimicifuga racemosa var. cordifolia", "title": "Cimicifuga racemosa var. cordifolia displays impressive long racemes of chalky-white blooms that resemble fluffy candles. ", "name": "CimicifugaRacemosaVar.Cordifolia" },
	"CimicifugaSimplex":
     { "header": "Cimicifuga simplex", "title": "Cimicifuga simplex", "name": "CimicifugaSimplex" },
	"CimicifugaSimplexAtropurpurea":
     { "header": "Cimicifuga simplex 'Atropurpurea'", "title": "Actaea simplex, formerly Cimicifuga simplex 'Atropurpurea' is an exceptionally beautiful selection. ", "name": "CimicifugaSimplexAtropurpurea" },
	"TheBlueGarden -AnnualFlowerMix":
     { "header": "The Blue Garden - Annual Flower Mix", "title": "Ranging from pale baby blue to deep midnight shades blue flowers are associated with serenity and calm", "name": "TheBlueGarden -AnnualFlowerMix" },
	"TheRedGarden -AnnualFlowerMix":
     { "header": "The Red Garden - Annual Flower Mix", "title": "In the garden red flowers are valued for the sharp contrast they produce. ", "name": "TheRedGarden -AnnualFlowerMix" },
	"TheWhiteGarden -AnnualFlowerMix":
     { "header": "The White Garden - Annual Flower Mix", "title": "As they say, white goes with everything and among theme gardens, the white garden may be the most popular. ", "name": "TheWhiteGarden -AnnualFlowerMix" }
};

export default flowers;
