'use strict';

// FAILED ATTEMPT TO ADD UNIT TESTS TO THIS PROJECT

import { jest } from "@jest/globals";
const Select = import ('./lw-select.js');

const selProps2 = {
  listSource: [
    { num: '1', nombre: 'NOROESTE' },
    { num: '2', nombre: 'LITORAL' },
    { num: '3', nombre: 'CUYO' },
    { num: '4', nombre: 'CENTRO' },
    { num: '5', nombre: 'PROVINCIA DE BUENOS AIRES' },
    { num: '6', nombre: 'CIUDAD DE BUENOS AIRES' },
    { num: '7', nombre: 'PATAGONIA' },
  ],
  onSelectionChanged: ( value, valueIndex, valueText ) => {
    console.log( 'selected:', value );
  },
  listDisplayProps: ( item, i ) => { return item.num + ' ' + item.nombre },
  listSearchProps: [ 'num', 'nombre' ],
  targetElementId: 'lwSelect2Here'
};

const jsdom = require( 'jsdom' );
const { JSDOM } = jsdom;
const dom = new JSDOM(
`<!DOCTYPE html>
<html>
  <body>
    <p>Hello world</p>
    <div id="lwSelect2Here"></div>
  </body>`);
</html>

const Select = require( './lw-select.js' );

const element = dom.window.document.getElementById('lwSelect2Here');
const s = new Select( element );
console.log( s.state );

test( 'always OK', () => { expect( true ).toBeTruthy(); });

describe('The genKey() function', () => {
  it( 'is a function', () => { expect( typeof genKey ).toBe( 'function' ); });
  it( 'returns a String', () => { expect( typeof genKey() ).toBe( 'string' ); });
  it( 'returns 12 digits', () => { expect( /\d{12}/.test( genKey())).toBeTruthy(); });
});

describe('The Select class', () => {
  it( 'is a function', () => { expect( typeof s ).toBe( 'function' ); });
});

