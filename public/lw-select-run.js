/*
 * Demo script
 * Instantiate the Select class, with different * numbers of
 * items: 7, 10K and 40K. 
 */

import carsListing from './carsListing.js';
import citiesListing from './citiesListing.js';
import Select from './lw-select/lw-select.js';

// props for the regions minimal example (7 items)
const selPropsRegions = {

  // The source of the list data, an array of objects.
  listSource: [
    { num: '1', nombre: 'NOROESTE' },
    { num: '2', nombre: 'LITORAL' },
    { num: '3', nombre: 'CUYO' },
    { num: '4', nombre: 'CENTRO' },
    { num: '5', nombre: 'PROVINCIA DE BUENOS AIRES' },
    { num: '6', nombre: 'CIUDAD DE BUENOS AIRES' },
    { num: '7', nombre: 'PATAGONIA' },
  ],

  // A function to be called whenever the selection is changed
  onSelectionChanged: ( value, valueIndex, valueText ) => {
    console.log( 'selected:', value );
  },

  // A function that returns the string to be displayed in the items list.
  listDisplayProps: ( item, i ) => { return item.num + ' ' + item.nombre },

  // The listSource property names that make a composite search field
  listSearchProps: [ 'num', 'nombre' ],

  // Make this Select read-only
  readonly: true,

  valueText: '6 CIUDAD DE BUENOS AIRES',

  targetElementId: 'lwSelectRegionsHere'
};

// props for the cars example (about 10K items)
const selPropsCars = {
  // The source of the list data, an array of objects.
  listSource: carsListing,

  // A function to be called whenever the selection is changed, either
  // assigned a value, or nullified, due to user actions.
  // Not called on initial values, when the control is initialized.
  // It will be passed 3 args: value, valueIndex and valueText
  // As this is a demo, the only use given to the selected item is to
  // display it in the UI.
  onSelectionChanged: ( value, valueIndex, valueText ) => {
    const thePane = document.querySelector( '#result pre' );
    thePane.innerText = ( valueIndex === null ) 
      ? 'no selection'
      : '#' + valueIndex + ' "' + valueText + '"\n'
      + JSON.stringify( value ).replace( /,/g, ', ' ).replace( /:/g, ': ' );
  },

  // A function that returns a composite field to be displayed for each item,
  // in the items list (the drop).
  // It must be an "arrow function" with two arguments named "item" and "i",
  // like so:
  // ( item, i ) => { '#' + i + ' ' + item.firstName + ' ' + item.secondName }
  // The argument named "item" refers to an item of the listSource array.
  // Instead of a function, it can be a string with the name of a single 
  // property present in the listSource objects array. In this case the
  // referenced field will be copied verbatim.
  listDisplayProps: ( item, i ) => { return item.Year + ' ' + item.Make + ' ' + item.Model },

  // The list of the listSource property names that make a composite search
  // field (lowercased and void of accents) for each item in the listsource
  // array.
  listSearchProps: [ 'Make', 'Model', 'Year', 'Category' ],

  // Placeholder text to be displayed in the search text input (optional)
  placeholder: 'type make, model, year, category',

  // Text to be used instead of the defaulr "no matches found"
  noMatchesMsg: 'no luck, zero matches',

  // Disabled or not
  disabled: false,

  // Make this Select not read-only
  readonly: false,

  // Text or key/id of a pre-selected item. If there is valueIndex, it overrides
  // valueText. Null if no initial value.
  value: null,
  valueText: null,
  valueIndex: 7777,

  // Drop-down arrow image, an SVG or IMG up to 24x24
  dropDownArrowIcon: `
    <svg class="lwButtonIcon" aria-hidden="true" viewBox="0 0 24 24" data-testid="ArrowDropDownIcon" stroke="#fff" fill="#fff">
      <path d="M7 10l5 5 5-5z"></path>
    </svg>
  `,

  // Only for the HTML version: id of the DOM element where this instance of 
  // lw-select has to be appended.
  targetElementId: 'lwSelectCarsHere',
};

// props for the cities example (about 40K items)
const selPropsCities = {
  // Cities example: a list of about 40K cities like this:
  // { city:'Tokyo', lat:'35.6897', long:'139.6922', country:'Japan', iso2:'JP', iso3:'JPN',
  // adminName:'Tōkyō', capital:'primary', population:'37977000', id:'1392685764' },
  listSource: citiesListing,
  onSelectionChanged: ( value, valueIndex, valueText ) => {
    const thePane = document.querySelector( '#result pre' );
    thePane.innerText = ( valueIndex === null ) 
      ? 'no selection'
      : '#' + valueIndex + ' "' + valueText + '"\n'
      + JSON.stringify( value ).replace( /,/g, ', ' ).replace( /:/g, ': ' );
  },
  listDisplayProps: ( item, i ) => { return item.city + ', ' + item.country + ' ' + item.iso2 + '/' + item.iso3 },
  listSearchProps: [ 'city', 'country', 'iso2', 'iso3', 'continent' ],
  placeholder: 'continent, country, city, ISO id',
  disabled: false,
  value: null,
  valueText: null,
  valueIndex: null,
  targetElementId: 'lwSelectCitiesHere',
};

const initializeRegions = ( event, selPropsRegions ) => {
  window.selRegions = new Select( selPropsRegions );
};

const initializeCars = ( event, selPropsCars ) => {
  window.selCars = new Select( selPropsCars );
};

const initializeCities = ( event, selPropsCities ) => {
  window.selCities = new Select( selPropsCities );
};

const loadRegionsHandler = ( event, arg ) => initializeRegions( event, arg );
const loadCarsHandler = ( event, arg ) => initializeCars( event, arg );
const loadHandlerCities = ( event, arg ) => initializeCities( event, arg );

window.addEventListener(
  'load',
  ( event ) => {
    loadRegionsHandler( event, selPropsRegions );
    loadCarsHandler( event, selPropsCars );
    loadHandlerCities( event, selPropsCities );
  }
);
