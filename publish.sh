#! /bin/bash

    printf "\n";
    read -n 1 -p "Dry run? [Y|n] " DRY
    echo "";
    case "$DRY" in
      [yY]|"") export DRYRUN="true" ;;
      *) export DRYRUN="false" ;;
    esac;
    echo "DRYRUN: "$DRYRUN;

    printf "\n";
    printf "version level: 1=patch, 2=minor, 3=major";
    echo "";
    read -n 1 -p "Choose level: [1|2|3] " LEVEL;
    echo "";
    case "$LEVEL" in
      [3]) export LEVEL="major" ;;
      [2]) export LEVEL="minor" ;;
      [1]|"") export LEVEL="patch" ;;
      *) echo "Invalid option"; exit; ;;
    esac;
    echo "LEVEL: "$LEVEL;

    printf "\n";
    export TITLE=$(grep '"title"' publishData.json | sed 's/"title": *"\(.*\)",/\1/')
    echo "TITLE: "$TITLE
    printf "enter the new title:";
    printf "\n";
    read -e -i "${TITLE}" TITLE
    echo "edited TITLE: "$TITLE

    if [ $DRYRUN == 'false' ]
    then
      printf "\n";
      read -n 1 -p "OK to go? [y|N] " OKTOGO
      echo "";
      case "$OKTOGO" in
        [yY]) export OKTOGO="true" ;;
        *) exit 1 ;;
      esac;
    fi

    printf "\n";
    read -n 6 -p "enter npm OTP: " NPMOTP;
    export NPMOTP=$NPMOTP;
    
    printf "\n";
    echo "npm OTP: "$NPMOTP;

    node ./publish.js
